from threading import Thread
import time

class Life(Thread):
    def __init__(self, name, lifespan):
        Thread.__init__(self)
        self.name = name
        self.lifespan = lifespan
        self.age = 1
    def run(self):
        print(f"{self.name} born")
        while self.age <= self.lifespan:
            time.sleep(1)
            print(f"{self.name}: {self.age}")
            self.age += 1
        print(f"{self.name} died")


planet = [
    Life('Mosquito', 1), # Instance Object
    Life('Housefly', 4),
    Life('Butterfly', 2)
]

if __name__ == "__main__":
    tic = time.time()
    for life in planet: # Start พร้อมกัน
        life.start()

    for life in planet: # wait for above process run complete and then run below
        life.join()

    toc = time.time()
    print("main process time: {}".format(toc - tic))