# Running Multiple Task or Coroutine
import time
# Example 3 method
## `asyncio.gather()` --> run by order, can't setup timeout, ไม่สามารถรับค่าเป็น list ของ coroutine/task ได้โดยตรง ใช้ * หน้า list เพื่อกระจายเป็น argument แทน
# uncommentd for run this
'''
import asyncio
async def cook(food, t):
    print(f'Microwave ({food}): Cooking {t} seconds...')
    await asyncio.sleep(t)
    print(f'Microwave ({food}): Finished cooking')
    return f'{food} is completed'

async def main():
    coros = [cook('Rice', 5), cook('Noodle', 3), cook('Curry', 1)]
    result = await asyncio.gather(*coros)
    print(result)

if __name__ == '__main__':
    t1 = time.time()
    asyncio.run(main())
    t2 = time.time() - t1
    print(f'Executed in {t2:0.2f} seconds.')
'''

## `asyncio.wait()` --> No order, can setup timeoit, รับค่า coroutine/task เป็น List ได้
## `DeprecationWarning: The explicit passing of coroutine objects to asyncio.wait() is deprecated since Python 3.8, and scheduled for removal in Python 3.11.`
'''
import asyncio
async def cook(food, t):
    print(f'Microwave ({food}): Cooking {t} seconds...')
    await asyncio.sleep(t)
    print(f'Microwave ({food}): Finished cooking')
    return f'{food} is completed'

async def main():
    coros = [cook('Rice', 5), cook('Noodle', 3), cook('Curry', 1)]
    results = await asyncio.wait(coros, return_when='ALL_COMPLETED', timeout=0.5) # if timeout หมดแล้ว แต่ยังไม่มี task ไหนเสร็จก็จะหมดเวลาเลย, result return เป็น tuple(set('Completed'), set('Uncompleted'))
    print(f'Completed task: {len(results[0])}')
    for completed_task in results[0]:
        print(f' - {completed_task.result()}')
    print(f'Uncompleted task: {len(results[1])}')

if __name__ == '__main__':
    asyncio.run(main())
'''

## `asyncio.as_completed()` --> No order, จะใช้กับ for Loop ซึ่งไม่เหมือนตัวอื่น และเป็น for Loop ที่ทุกรอบเริ่มทำงานพร้อมกัน
'''
import asyncio
async def cook(food, t):
    print(f'Microwave ({food}): Cooking {t} seconds...')
    await asyncio.sleep(t)
    print(f'Microwave ({food}): Finished cooking')
    return f'{food} is completed'

async def main():
    coros = [cook('Rice', 5), cook('Noodle', 3), cook('Curry', 1)]
    for coro in asyncio.as_completed(coros):
        result = await coro
        print(result)

if __name__ == '__main__':
    asyncio.run(main())
'''