# Create task from coroutine
import asyncio

async def wash(basket):
    print(f'Washing Machine ({basket}): Put the coin')
    print(f'Washing Machine ({basket}): Start washing...')
    await asyncio.sleep(5)
    print(f'Washing Machine ({basket}): Finished washing')
    return f'{basket} is completed'

async def main():
    coro = wash('Basket A') # Coroutine Object
    print(coro)
    print(type(coro))
    # use `asyncio.create_task('task_name')` method for create task from coroutine object
    task = asyncio.create_task(coro) # This is a task
    print(task)
    print(type(task))
    result = await task # await ordered task, result --> received return value from function
    print(result)

if __name__ == '__main__':
    asyncio.run(main())